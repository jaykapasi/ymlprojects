package yml.newsapp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;

import com.android.volley.toolbox.NetworkImageView;

import java.util.logging.Logger;

import yml.newsapp.R;

public class News extends Activity {

    private static final Logger log = Logger.getLogger(News.class.getName());
    String u_id, user_name;
    ImageLoader mImageLoader;
    NetworkImageView mNetworkImageView;
    String category[][];
    Splash s;
    TextView name;
    ListView mDrawerList,newslist;
    JSONRequest json_request;
    GetData getData;
    int flag = 0, flag2 = 0,news_pointer=0,flag3=0;
    private DrawerLayout mDrawerLayout;
    ActionBarDrawerToggle mDrawerToggle;
    CharSequence mTitle,mDrawerTitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navigation_drawer);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        newslist = (ListView) findViewById(R.id.news_data);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        name = (TextView) findViewById(R.id.name);
        json_request = new JSONRequest(getApplicationContext());
        u_id = getIntent().getStringExtra("user_id");
        s = new Splash();
        user_name = getIntent().getStringExtra("user_name");
        name.setText(user_name);
        String url = "https://graph.facebook.com/" + u_id + "/picture";
        mNetworkImageView = (NetworkImageView) findViewById(R.id.networkImageView);
        mImageLoader = MySingletonClass.getInstance(this).getImageLoader();
        mNetworkImageView.setImageUrl(url, mImageLoader);

        mImageLoader.get(url, new ImageLoader.ImageListener() {

            public void onErrorResponse(VolleyError arg0) {
                // image.setImageResource(R.drawable.icon_error); // set an error image if the download fails
            }

            public void onResponse(ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    mNetworkImageView.setImageBitmap(CircleImage.getRoundedRectBitmap(response.getBitmap(), 50));
                }
            }
        });

        final String URL = "https://devru-times-of-india.p.mashape.com/feeds/feedurllist.cms?catagory=city";

        getData = new GetData();
        getData.sendRequest(URL, "Item", "name", "sectionurl");

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                news_pointer = position;
                getData.sendRequest(URL, "Item", "name", "sectionurl");
                flag2 = 0;
                mDrawerLayout.closeDrawers();
                Log.d("POsition", position + "");

            }
        });
        newslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(News.this, Detail.class);
                i.putExtra("newsurl", category[position][1]);
                startActivity(i);
            }
        });


        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.news, menu);
        MenuItem item = menu.findItem(R.id.Logout);
        item.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                s.logout();
                Intent i=new Intent(News.this,Splash.class);
                startActivity(i);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if(flag3==0) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
            flag3=1;
        }
        else{
            mDrawerLayout.closeDrawers();
            flag3=0;
        }

        return super.onOptionsItemSelected(item);
    }

    class GetData implements JSONRequest.LoadData {
        @Override
        public void onDataReceived(String[][] data) {

            if(flag==0) {
                if (data != null) {
                    mDrawerList.setAdapter(new ArrayAdapter<String>(News.this, R.layout.listview_item, data[0]));

                }
                flag = 1;
            }

            if (flag == 1 && flag2==0) {
                sendRequest(data[1][news_pointer], "Item", "category", "url");
                log.info("onDataReceived called");

                flag2=1;
            }
            else if (flag == 1 && flag2 == 1) {
                sendRequest1(data[1][0], "NewsItem", "HeadLine", "WebURL", "Image");
                Log.d("url checking",data[1][0]);
                log.info("onDataReceived called-----------");

            }


        }

        @Override
        public void onNewsReceived(String[][] news) {
            category=news;
            Log.d("OnNewsreceived","------------------------");
            if(news!=null)
            newslist.setAdapter(new ItemAdapter(news));

        }
         @Override
         public void sendRequest(String url,String item,String name,String sectionurl) {
            json_request = new JSONRequest(this);
            json_request.fetchJSON(url, item, name, sectionurl);

        }
        @Override
        public void sendRequest1(String url, String item, String name, String sectionurl, String image) {
            json_request = new JSONRequest(GetData.this);
            json_request.fetchJSONNews(url, item, name, sectionurl, image);
        }

    }

    private static class ViewHolder {
        TextView title;
        NetworkImageView image;
    }

    class ItemAdapter extends BaseAdapter {
        String news[][];
        ItemAdapter(String[][] news){
            this.news=news;
        }

        //	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

        @Override
        public int getCount() {

            return news.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;
            final ViewHolder holder;
            if (convertView == null) {
                view = getLayoutInflater().inflate(R.layout.newslistview_data, parent, false);
                holder = new ViewHolder();
                holder.title = (TextView) view.findViewById(R.id.title);
                holder.image = (NetworkImageView) view.findViewById(R.id.news_image);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.title.setText(news[position][0]);
            mImageLoader = MySingletonClass.getInstance(getApplicationContext()).getImageLoader();
            holder.image.setImageUrl(news[position][2], mImageLoader);
            //imageLoader.displayImage(s1[position][2], holder.image, options, null);

            return view;
        }
    }
}



