package yml.newsapp;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Created by rahulritesh on 11/12/14.
 */
public class Detail extends Activity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_news);

        String url=getIntent().getStringExtra("newsurl");
        webView=(WebView)findViewById(R.id.detail_news);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }
}
