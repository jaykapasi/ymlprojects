package yml.newsapp;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import yml.newsapp.MySingletonClass;
import yml.newsapp.R;

/**
 * Created by rahulritesh on 9/12/14.
 */
public class JSONRequest {
    Context context;
    private LoadData loadData;
    String catagory[][];

    JSONRequest(Context context)
    {

        this.context=context;
    }
    JSONRequest(LoadData loadData)
    {

        this.loadData=loadData;
    }

    public void fetchJSON(String url, final String item, final String name, final String sectionurl) {
        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // handle response
                        try {
                            JSONArray jarray = (JSONArray) response.get(item);
                            catagory = new String[2][jarray.length()];
                            //catagory_name=new String[jarray.length()];
                            for (int i = 0; i < jarray.length(); i++) {
                                JSONObject jobj = (JSONObject) jarray
                                        .get(i);
                                catagory[1][i] = jobj.getString(sectionurl);
                                catagory[0][i] = jobj.getString(name);
                            }

                            loadData.onDataReceived(catagory);
                        } catch (JSONException je) {
                            //loadData.onError("Volley Error JSONException: " + je.getMessage());
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // handle error
                //loadData.onError("Volley Error: " + error.getMessage());
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Mashape-Key", "l7p5vxCMRNmshfCz9DUJRbOSD9MMp18pAlhjsn6dYERUUwP9SN");

                return headers;
            }

        };
         // Access the RequestQueue through your singleton class.
        MySingletonClass.getInstance(context).addToRequestQueue(req);
    }

    public void fetchJSONNews(String url, final String item, final String name, final String sectionurl,final String image) {
        JsonObjectRequest req = new JsonObjectRequest(url, new JSONObject(),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // handle response
                        try {
                            JSONArray jarray=response.getJSONArray(item);
                            catagory=new String[jarray.length()][3];
                            for(int i=0;i<jarray.length();i++){
                                JSONObject jobj = (JSONObject) jarray
                                        .get(i);
                                catagory[i][0]=jobj.getString(name);
                                catagory[i][1]=jobj.getString(sectionurl);
                               JSONObject img= jobj.getJSONObject(image);

//                                if(img!=null)
                                catagory[i][2]=img.getString("Thumb");
                               // else
                               // catagory[i][2]="http://timesofindia.indiatimes.com/thumb.cms?photoid=45452393";
                                Log.d("Business Data",catagory[i][0]);
                            }
                            loadData.onNewsReceived(catagory);
                        }
                            catch (JSONException je) {
                            //loadData.onError("Volley Error JSONException: " + je.getMessage());
                        }

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // handle error
                //loadData.onError("Volley Error: " + error.getMessage());
            }
        }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("X-Mashape-Key", "l7p5vxCMRNmshfCz9DUJRbOSD9MMp18pAlhjsn6dYERUUwP9SN");

                return headers;
            }

        };
        // Access the RequestQueue through your singleton class.
        MySingletonClass.getInstance(context).addToRequestQueue(req);
    }


    public interface LoadData {
        public void onDataReceived(String[][] data);

        public void onNewsReceived(String[][] news);

        public void sendRequest(String url,String item,String name,String sectionurl);
        public void sendRequest1(String url, String item, String name, String sectionurl, String image);

    }
}
