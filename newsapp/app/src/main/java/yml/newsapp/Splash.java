package yml.newsapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.UiLifecycleHelper;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import java.util.Arrays;
import com.facebook.Request;
import com.facebook.Response;


public class Splash extends Activity {


    private UiLifecycleHelper uiHelper;
    String check = "";

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            // onSessionStateChange(session, state, exception);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        LoginButton authButton = (LoginButton) findViewById(R.id.authButton);

        authButton.setReadPermissions(Arrays.asList("public_profile", "email"));

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

    }

  @SuppressWarnings("deprecation")
    private void onSessionStateChange(Session session, SessionState state,
                                      Exception exception) {
        if (state.isOpened()) {

            Request.executeMeRequestAsync(session,
                    new Request.GraphUserCallback() {

                        @Override
                        public void onCompleted(GraphUser user,
                                                Response response) {

                            if (user != null) {
                                Intent i = new Intent(Splash.this,News.class);
                                i.putExtra("user_id",user.getId().toString());
                                i.putExtra("user_name",user.getFirstName()+" "+user.getLastName());
                                startActivity(i);
                                finish();
                            }
                        }
                    });


        } else if (state.isClosed()) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null && (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }
        uiHelper.onResume();
    }

   @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }
    public void logout(){
        // clear any user information
       // mApp.clearUserPrefs();
        // find the active session which can only be facebook in my app
        Session session = Session.getActiveSession();
        // run the closeAndClearTokenInformation which does the following
        // DOCS : Closes the local in-memory Session object and clears any persistent
        // cache related to the Session.
        session.closeAndClearTokenInformation();
        // return the user to the login screen
        //startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        // make sure the user can not access the page after he/she is logged out
        // clear the activity stack
        finish();
    }

}


